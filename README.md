# Bewerbung Usecase

## Aufgabe 1

See [aufgabe1.pdf](aufgabe1.pdf)

## Aufgabe 2

Start the development server.

```console
poetry run uvicorn aufgabe2:app --reload
```

The Mini-API should be running on http://127.0.0.1:8000.

To see an overview of the endpoints and test the API go to http://127.0.0.1:8000/docs.

Run all tests:

```console
poetry run pytest
```
