import json
from collections import defaultdict
from pathlib import Path
from typing import Dict, List, Literal

from fastapi import FastAPI, HTTPException
from pydantic import BaseModel

base_dir = Path(__file__).parent


class Node(BaseModel):
    id: int
    text: str
    dialogId: int
    type: Literal["question", "answer"]


class Edge(BaseModel):
    parentId: int
    answer: str
    childId: int


class Dialog(BaseModel):
    id: int
    title: str


class Answer(BaseModel):
    text: str


def is_root_node(node: Node) -> bool:
    return all(node.id != edge.childId for edge in edges)


nodes = [Node(**x) for x in json.loads((base_dir / "nodes.json").read_text())]
edges = [Edge(**x) for x in json.loads((base_dir / "edges.json").read_text())]
dialogs = [Dialog(**x) for x in json.loads((base_dir / "dialogs.json").read_text())]

dialogs_by_id = {dialog.id: dialog for dialog in dialogs}
nodes_by_dialog_id = defaultdict(dict)
for node in nodes:
    nodes_by_dialog_id[node.dialogId][node.id] = node

edges_by_nodes_id = defaultdict(dict)
for edge in edges:
    edges_by_nodes_id[edge.parentId][edge.answer] = edge


app = FastAPI()


@app.get("/dialogs")
async def get_dialogs() -> List[Dialog]:
    return dialogs


@app.get("/dialogs/{dialog_id}")
async def get_dialog(dialog_id: int) -> Dialog:
    return dialogs_by_id[dialog_id]


@app.get("/dialogs/{dialog_id}/root")
async def get_root_node(dialog_id: int) -> Node:
    return next(filter(is_root_node, nodes_by_dialog_id[dialog_id].values()))


@app.get("/dialogs/{dialog_id}/nodes")
async def get_nodes(dialog_id: int) -> Dict[int, Node]:
    return nodes_by_dialog_id[dialog_id]


@app.get("/dialogs/{dialog_id}/nodes/{node_id}")
async def get_node(dialog_id: int, node_id: int) -> Node:
    return nodes_by_dialog_id[dialog_id][node_id]


@app.get("/dialogs/{dialog_id}/nodes/{node_id}/answers")
async def get_node_answers(dialog_id: int, node_id: int) -> List[str]:
    return [edge.answer for edge in edges if edge.parentId == node_id]


@app.post("/dialogs/{dialog_id}/nodes/{node_id}")
async def post_node_answer(dialog_id: int, node_id: int, answer: Answer) -> int:
    "Returns the ID of the next node."
    if node_id not in edges_by_nodes_id:
        raise HTTPException(status_code=400, detail="This node has no answers.")

    edges_dict = edges_by_nodes_id[node_id]
    if answer.text not in edges_dict:
        raise HTTPException(status_code=400, detail="Invalid answer.")
    return edges_dict[answer.text].childId
