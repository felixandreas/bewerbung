import pytest
from httpx import AsyncClient

from aufgabe2 import app


@pytest.yield_fixture()
async def async_client():
    async_client = AsyncClient(app=app, base_url="http://test")
    yield async_client
    await async_client.aclose()


@pytest.mark.asyncio
async def test_get_dialogs(async_client):
    response = await async_client.get("/dialogs")

    assert response.status_code == 200
    assert response.json() == [
        {"id": 1, "title": "Rätsel"},
        {"id": 2, "title": "Rechenaufgabe"},
    ]


@pytest.mark.asyncio
async def test_get_dialog(async_client):
    response = await async_client.get("/dialogs/1")
    assert response.status_code == 200
    assert response.json() == {"id": 1, "title": "Rätsel"}


@pytest.mark.asyncio
async def test_get_root_node(async_client):
    response = await async_client.get("/dialogs/2/root")
    assert response.status_code == 200
    assert response.json() == {
        "id": 4,
        "text": "Darf ich dir eine Rechenaufgabe stellen?",
        "dialogId": 2,
        "type": "question",
    }


@pytest.mark.asyncio
async def test_get_node(async_client):
    response = await async_client.get("/dialogs/2/nodes/105")
    assert response.status_code == 200
    assert response.json() == {
        "id": 105,
        "text": "Okay, dann nicht",
        "dialogId": 2,
        "type": "answer",
    }


@pytest.mark.asyncio
async def test_get_node_answers(async_client):
    response = await async_client.get("/dialogs/1/nodes/2/answers")
    assert response.status_code == 200
    assert response.json() == ["Ja", "Nein", "Naja"]


@pytest.mark.asyncio
async def test_post_node_answer(async_client):
    response = await async_client.post("/dialogs/1/nodes/2", json={"text": "Naja"})
    assert response.status_code == 200
    assert response.json() == 105
